#include <substrate.h>
#include <stdio.h>
#include <stdlib.h>
#import <mach-o/dyld.h>
#import <Foundation/Foundation.h>


%hook SBDashBoardMediaControlsViewController
-(void)viewDidLoad {

  [[self view] setBackgroundColor:[UIColor clearColor]];
  UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];

  UIVisualEffectView *blurView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
  blurView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.55];
  blurView.clipsToBounds = YES;
  blurView.layer.cornerRadius = 6.0;

  blurView.frame = [[self view] bounds];
  [[self view] addSubview:blurView];

  %orig;

  // NSArray* views = [[[[[[self view] subviews] objectAtIndex:1] subviews] objectAtIndex:0] subviews];
}
%end

%hook MediaControlsHeaderView
-(UILabel *)primaryLabel {
  UILabel* label = %orig;
  label.textColor = [UIColor blackColor];
  return label;
}
-(UILabel *)titleLabel {
  UILabel* label = %orig;
  label.textColor = [UIColor blackColor];
  return label;
}
-(UILabel *)secondaryLabel {
  UILabel* label = %orig;
  label.textColor = [UIColor blackColor];
  return label;
}
%end